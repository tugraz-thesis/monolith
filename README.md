# Monolithic demo application

All services are packed in one single .war artifact and the entire
application runs as a single process on the web server Wildfly.
Use the .sh (Linux) or .bat (Windows) scripts to build and deploy the application.


## Used technologies

- GWT 2.8.2
- Errai 4.3.0
- Wildfly 11.0.0 web server
- Java Oracle JDK 1.8
- Maven 3.x
- Amazon Alexa Java SDK 2.5.4

The project is based on the official errai tutorial and the Amazon Alexa SDK Tutorial:
* https://github.com/errai/errai-tutorial (Copyright 2016 Red Hat, Inc. and/or its affiliates.)
* https://github.com/alexa/skill-sample-java-fact (Copyright 2018 Amazon.com)


## Prerequisites

To build the single .war artifact:
 ```
  ./create-war.sh
 ```

To deploy .war artifact to Wildfly web sever

 ```
  ./deploy-only.sh
 ```
Add your Amazon Skill ID to the config (Security Issue, otherwise you cannot use your Skill globally in the Skill Store)

NewAlexaSkillStorageServiceImpl.java:
 ```
  e.g. amzn1.ask.skill.4e9e1234-yyyy-xxxx-bae7-56564be091a
 ```


## Running the application

You can open the app welcome page via
https://localhost:8443/monolith


HTTP endpoint:
https://localhost:8443/monolith/rest/newskill

**For testing:**
Use the example JSON input file as payload body and send a POST request to this HTTP Endpoint.




## Wildfly configuration

Use the configuration file in the config/ folder.

Alternatively, you can add the database configuration manually:

 ```
                <datasource jta="true" jndi-name="java:jboss/datasources/PostgresDS" pool-name="PostgresDS" enabled="true" use-ccm="false">
                    <connection-url>jdbc:postgresql://cryptcurrency-fact-postgres-instance.cmohhm8wqb9p.eu-west-1.rds.amazonaws.com:5432/cryptcurrency_fact_postgres_db</connection-url>
                    <driver-class>org.postgresql.Driver</driver-class>
                    <driver>postgresql-42.2.4.jar</driver>
                    <security>
                        <user-name>postgres_user</user-name>
                        <password>xxx</password>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker"/>
                        <background-validation>true</background-validation>
                        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter"/>
                    </validation>
                </datasource>
 ```

 ```
                 <datasource jta="false" jndi-name="java:jboss/datasources/MySQLDS" pool-name="MySqlDS">
                     <connection-url>jdbc:mysql://cryptcurrency-fact-mysql-instance.cmohhm8wqb9p.eu-west-1.rds.amazonaws.com:3306/cryptcurrency_fact_mysql_db?useSSL=false&amp;useUnicode=yes&amp;characterEncoding=UTF-8&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC</connection-url>
                     <driver>mysql</driver>
                     <security>
                         <user-name>mysql_user</user-name>
                         <password>xxx</password>
                     </security>
                     <validation>
                         <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
                         <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"/>
                     </validation>
                 </datasource>

 ```
  ```
                 <drivers>
                      <driver name="h2" module="com.h2database.h2">
                          <xa-datasource-class>org.h2.jdbcx.JdbcDataSource</xa-datasource-class>
                      </driver>
                      <driver name="mysql" module="com.mysql">
                          <driver-class>com.mysql.cj.jdbc.Driver</driver-class>
                      </driver>
                      <driver name="postgresql" module="com.postgres">
                          <driver-class>org.postgresql.Driver</driver-class>
                      </driver>
                 </drivers>

   ```

## Monolithic Application Overview

It's a GWT application with client, shared and server code.

**src/main/java/client/local:**
Here you can find the welcome page.


**src/main/java/client/shared:**
Here you can find all shared objects between client and server.
e.g. db entities and interface for the HTTP endpoints.


**src/main/java/server:**
Here you can find the server code. The implemented HTTP endpoint and the database access layer.


**NewAlexaSkillStorageService** it's the entry point for the entire application. It's the HTTP endpoint 'https://localhost:8443/monolith/rest/newskill'
Takes the JSON data as InputStream, parse the JSON to 'requestEnvelope' and creates a new JSON via FactIntentHandler.



## Summary

The entire monolithic application are stored in this folder. You can deploy the application to any server instance with a running Wildfly webserver.
