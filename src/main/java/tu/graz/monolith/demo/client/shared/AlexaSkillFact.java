package tu.graz.monolith.demo.client.shared;

import javax.persistence.*;

@Entity
@Table(name="alexa_crypto_fact")
@Cacheable
public class AlexaSkillFact {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;

    @Column(name="fact_text")
    private String factText;

    @Column(name="id_image_url")
    private long idUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFactText() {
        return factText;
    }

    public void setFactText(String factText) {
        this.factText = factText;
    }

    public long getIdUrl() {
        return idUrl;
    }

    public void setIdUrl(int idUrl) {
        this.idUrl = idUrl;
    }
}
