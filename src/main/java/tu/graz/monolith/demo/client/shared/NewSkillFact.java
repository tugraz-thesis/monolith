package tu.graz.monolith.demo.client.shared;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.Objects;

@Portable
@JsonIgnoreProperties(ignoreUnknown = true)
public class NewSkillFact {

    private String version;

    public NewSkillFact() {

    }

    public NewSkillFact(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "NewSkillFact{" +
                "version='" + version + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewSkillFact that = (NewSkillFact) o;
        return Objects.equals(version, that.version);
    }

    @Override
    public int hashCode() {

        return Objects.hash(version);
    }
}
