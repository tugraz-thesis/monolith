package tu.graz.monolith.demo.client.shared;


import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.io.InputStream;


@Path("/newskill")
public interface NewAlexaSkillStorageService {

    /**
     * @return A random Fact
     */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    Response getNewFact(InputStream input);





}