package tu.graz.monolith.demo.client.local;

import elemental2.dom.HTMLDocument;
import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.shared.api.annotations.Templated;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Page(role = DefaultPage.class, path = "/mypage")
@Templated(value = "my-page.html")
public class MyPage {

    @Inject
    private JQueryProducer.JQuery $;

    @Inject
    private HTMLDocument document;


    @PostConstruct
    private void setup() {
        
    }
}
