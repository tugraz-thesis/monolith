package tu.graz.monolith.demo.client.shared;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="alexa_fact_image_url")
@Cacheable
public class ImageUrl {

    @Id
    private long id;
    private String url;

    public ImageUrl(long id, String url) {
        this.id = id;
        this.url = url;
    }

    public ImageUrl() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setName(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Fact{" +
                "id=" + id +
                ", url='" + url + '\'' +
                '}';
    }
}
