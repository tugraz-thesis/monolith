package tu.graz.monolith.demo.server;


import com.amazon.ask.Skill;
import com.amazon.ask.Skills;
import com.amazon.ask.model.RequestEnvelope;
import com.amazon.ask.model.ResponseEnvelope;
import com.amazon.ask.model.services.Serializer;
import com.amazon.ask.util.JacksonSerializer;
import tu.graz.monolith.demo.client.shared.NewAlexaSkillStorageService;
import tu.graz.monolith.demo.server.handler.*;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.io.InputStream;


@Stateless
public class NewAlexaSkillStorageServiceImpl implements NewAlexaSkillStorageService {

    @Inject
    AlexaSkillFactEntityService alexaSkillFactEntityService;

    @Override
    public Response getNewFact(InputStream input) {

        final Serializer serializer = new JacksonSerializer();
        RequestEnvelope requestEnvelope = (RequestEnvelope)serializer.deserialize(input, RequestEnvelope.class);


        Skill mySkill = Skills.standard()
                .addRequestHandlers(
                        new CancelandStopIntentHandler(),
                        new FactIntentHandler(alexaSkillFactEntityService),
                        new HelpIntentHandler(),
                        new LaunchRequestHandler(),
                        new SessionEndedRequestHandler(),
                        new FallBackIntentHandler())
                // Add your skill id below
                .withSkillId("amzn1.ask.skill.4e9e9096-382c-499d-bae7-773054be091a")
                .build();


        ResponseEnvelope response = mySkill.invoke(requestEnvelope);

        String jsonString = serializer.serialize(response);


        return Response.ok(jsonString).build();
    }
}
