package tu.graz.monolith.demo.server.handler;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;

import java.util.Optional;

import static com.amazon.ask.request.Predicates.intentName;

/*
    Skill Sample Java Fact
    Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 */

public class HelpIntentHandler implements RequestHandler {

    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(intentName("AMAZON.HelpIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        String speechText = "I can tell you an cryptocurrency fact. Try saying tell me an cryptocurrency fact";
        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("Cryptocurrency Facts", speechText)
                .withReprompt(speechText)
                .build();
    }

}
