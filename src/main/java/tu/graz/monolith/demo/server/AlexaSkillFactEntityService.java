/**
 * Copyright (C) 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tu.graz.monolith.demo.server;

import tu.graz.monolith.demo.client.shared.AlexaSkillFact;
import tu.graz.monolith.demo.client.shared.ImageUrl;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class AlexaSkillFactEntityService {


  @PersistenceContext(unitName = "ds-postgresql")
  private EntityManager emPDS;


  @PersistenceContext(unitName = "ds-mysql")
  private EntityManager emMDS;


  public AlexaSkillFactEntityService(){};


  public AlexaSkillFact getFactById (final long id){

    final AlexaSkillFact fact = emPDS.find(AlexaSkillFact.class, id);

    if (fact != null) {
      return fact;
    } else {
      throw new IllegalArgumentException(
              "The given id, " + id + ", was not a key for any " + AlexaSkillFact.class.getSimpleName());

    }
  }

  public long getFactSize() {

    long size = (long) emPDS.createQuery("SELECT COUNT(f.id) FROM AlexaSkillFact f").getSingleResult();

    return size;

  }


  public ImageUrl testUrlById (final long id){

    final ImageUrl url = emMDS.find(ImageUrl.class, id);

    if (url != null) {
      return url;
    } else {
      throw new IllegalArgumentException(
              "The given id, " + id + ", was not a key for any " + ImageUrl.class.getSimpleName());

    }
  }


  public ImageUrl getUrlById (final long id){

    final ImageUrl url = emMDS.find(ImageUrl.class, id);

    if (url != null) {
      return url;
    } else {
      throw new IllegalArgumentException(
              "The given id, " + id + ", was not a key for any " + ImageUrl.class.getSimpleName());

    }
  }

}
